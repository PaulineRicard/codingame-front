import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserAdminComponent} from './component/user-admin/user-admin.component';
import {AppComponent} from './app.component';
import {LangageAdminComponent} from './component/langage-admin/langage-admin.component';
import {ExoAdminComponent} from './component/exo-admin/exo-admin.component';
import {AddExoComponent} from './component/add-exo/add-exo.component';
import {EditorComponent} from './component/editor/editor.component';
import {HomeUserComponent} from './component/home-user/home-user.component';


const routes: Routes = [
  {
    path: 'admin/users',
    component: UserAdminComponent,
  },
  {
    path: 'admin/langages',
    component: LangageAdminComponent,
  },
  {
    path: 'admin/exos',
    component: ExoAdminComponent,
  },
  {
    path: 'admin/addexo',
    component: AddExoComponent,
  },
  {
    path: 'logout',
    component: AppComponent,
  },
  {
    path: '',
    component: AppComponent,
  }, {
    path: 'editor/:id',
    component: EditorComponent,
  },{
    path: 'userhome',
    component: HomeUserComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
