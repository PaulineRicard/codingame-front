import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserAdminComponent } from './component/user-admin/user-admin.component';
import { NavbarAdminComponent } from './component/navbar-admin/navbar-admin.component';
import { LangageAdminComponent } from './component/langage-admin/langage-admin.component';
import { ExoAdminComponent } from './component/exo-admin/exo-admin.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbButtonsModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import { ModalUpdateUserComponent } from './component/modal-update-user/modal-update-user.component';
import {FormsModule} from '@angular/forms';
import { AddExoComponent } from './component/add-exo/add-exo.component';
import { HttpClientModule } from '@angular/common/http';
import { EditorComponent } from './component/editor/editor.component';
import { HomeUserComponent } from './component/home-user/home-user.component';

@NgModule({
  declarations: [
    AppComponent,
    UserAdminComponent,
    NavbarAdminComponent,
    LangageAdminComponent,
    ExoAdminComponent,
    ModalUpdateUserComponent,
    AddExoComponent,
    EditorComponent,
    HomeUserComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NgbCollapseModule,
    NgbButtonsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
