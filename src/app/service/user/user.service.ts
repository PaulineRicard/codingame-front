import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  apiURL = 'http://127.0.0.1:3030';
  constructor(private http: HttpClient, private router: Router) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // // récupère tous les users de la bdd !!
  getAllUsers() {
    return this.http.get(this.apiURL + '/users').toPromise();
  }

  updateUser(user) {
    console.log(user);
  // todo voir format envoi des datas?? erreur pour l'update !!!!!!
    return this.http.put(this.apiURL + '/users', user, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }), responseType: 'text'
    }).toPromise();
  }
}
