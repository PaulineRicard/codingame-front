import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ExoService {

  constructor(private http: HttpClient) { }

  createExo(exercice) {
    console.log('exo');
    // rempli la table exo pour chaque langage select
    return this.http.post('http://127.0.0.1:3030/exo/create', exercice, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }), responseType: 'text'
    }).toPromise();
  }
   createValue() {
    console.log('value');
  }
  createResult() {
    console.log('correction');
  }
  createCorrection() {
    console.log('Correction');
  }

  createEntrie(val: any) {
    return this.http.post('url', val, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }), responseType: 'text'
    }).toPromise();
}

  createSortie(val: any) {
    return this.http.post('url', val, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }), responseType: 'text'
    }).toPromise();
  }

  saveEnd(idExo: any, idLg: any, idE: any, idS: any) {
    const val = {
      idexo: idExo,
    }
    return this.http.post('url', val, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }), responseType: 'text'
    }).toPromise();
  }


  getByLg(id: number) {
    return this.http.get(`http://127.0.0.1:3030/exo/langage/${id}`).toPromise();
  }

  loadExo(exoId: any) {
    return this.http.get('127.0.0.1:3030/exo/' + exoId).toPromise();
  }
}


