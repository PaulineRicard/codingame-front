import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LangageService {

  apiURL = 'http://127.0.0.1:3030';
  constructor(private http: HttpClient, private router: Router) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getAll() {
    return this.http.get(this.apiURL + '/langages').toPromise();
  }
  create(body) {
    // console.log('create !!!!!!!!!!!');
    return this.http.post(this.apiURL + '/langages', body, {headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })}).toPromise();
  }
  // create() {
  //   console.log('create !!!!!!!!!!!');
  // }
}
