import { Component, OnInit } from '@angular/core';
import {ExoService} from '../../service/exo/exo.service';

@Component({
  selector: 'app-add-exo',
  templateUrl: './add-exo.component.html',
  styleUrls: ['./add-exo.component.css']
})
export class AddExoComponent implements OnInit {
  value = [];
  correctionvalue = [];
  ennonce: string;
  langages = [
    {id: 1,
    name: 'php',
      checked: false,
      btn: false
    },
    {
      id: 2,
      name: 'javascript',
      checked: false,
      btn: false
    }
  ];
  langagesChecked = [];
  name: string;
  sortie: any;
  entrie: any;
  isValid = true;
  i = 0;
  constructor(private exoService: ExoService) { }

  ngOnInit() {

  }
  // au clic sur le bouton add valeur de test: recupère les langages cochés et créer les input pour entrer les valeurs des tests par langage
  addValeurTest() {
    // todo recuperer langages via le service !!!
    console.log(this.langages);
    this.langagesChecked = [];
    for (const langage of this.langages) {
      if (langage.checked === true) {
        // @ts-ignore
       this.langagesChecked.push(langage);
      }
    }
    console.log(this.langagesChecked);
  }
  addlangage(langage) {
    // console.log('this.langage.id');
    langage.checked = !langage.checked;
  }

  saveAll() {
    // TODO recupérer toutes les valeurs + les envoyées au service => api pour insert dans les tables !!!
    for (const langage of this.langagesChecked) {
      const exercice = {
        name: this.name,
        ennonce: this.ennonce,
        id_langage: langage.id
      }
      this.exoService.createExo(exercice).then(result => {
  console.log(result);
  const idExo = parseInt(result, 10);
  this.saveEntrieAndSortie(langage);
  let j = 0
  this.exoService.createEntrie(this.value[j]).then(result2 => {
    const entrieId = result2;
    this.exoService.createSortie(this.correctionvalue[j]).then(result3 => {
      const sortieId = result3;
      this.saveEnd(idExo, langage.id, entrieId, sortieId);
    });
  });
  j++;
  });
}
  }
  private saveEntrieAndSortie(langage) {
    this.value[(this.i)] = this.entrie;
    this.correctionvalue[this.i] = this.sortie;
    this.isValid = false;
    console.log(this.value);
    this.i++;
  }

  private saveEnd(idExo, idLg, idE, idS) {
    this.exoService.saveEnd(idExo, idLg, idE, idS).then(res => {
      console.log(res);
    });
  }
}
