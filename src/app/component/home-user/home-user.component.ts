import { Component, OnInit } from '@angular/core';
import {ExoService} from '../../service/exo/exo.service';
import {LangageService} from '../../service/langage/langage.service';
import {consoleTestResultHandler} from 'tslint/lib/test';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home-user',
  templateUrl: './home-user.component.html',
  styleUrls: ['./home-user.component.css']
})
export class HomeUserComponent implements OnInit {
  langages: any;
  exos;

  constructor(private serviceExo: ExoService, private serviceLg: LangageService, private router: Router) { }

  ngOnInit() {
    this.loadLangages();
  }

  loadExo(id: number) {
    this.serviceExo.getByLg(id).then(result => {
      this.exos = result;
      console.log(result);
      console.log('###############');
    });
  }

  private loadLangages() {
    this.serviceLg.getAll().then(result => {
      this.langages = result;
    });
  }

  routeExo(id: any) {
    this.router.navigate(['editor', id]).then(r => console.log(r));
  }
}
