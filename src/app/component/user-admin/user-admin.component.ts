import { Component, OnInit } from '@angular/core';
import {UserService} from '../../service/user/user.service';

@Component({
  selector: 'app-home-admin',
  templateUrl: './user-admin.component.html',
  styleUrls: ['./user-admin.component.css']
})
export class UserAdminComponent implements OnInit {
  // TODO recuperer liste users via service!!
  public users;

  constructor(private userService: UserService) {
    // console.log(this.users);
  }

  ngOnInit() {
    console.log(this.users);
    console.log(this.userService);
    this.getUsers();
  }
  getUsers() {
    this.userService.getAllUsers().then(result => {
      console.log(result);
      this.users = result;
    }).catch(reason => {
      console.log(reason);
    });
  }
}
