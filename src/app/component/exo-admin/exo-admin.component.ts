import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-exo-admin',
  templateUrl: './exo-admin.component.html',
  styleUrls: ['./exo-admin.component.css']
})
export class ExoAdminComponent implements OnInit {
  exos = [
    {id: 1,    name: 'exo1',
    ennonce: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugit eum placeat consequatur doloremque officiis ducimus.',
    langage: 'php'},
    {id: 2,
      name: 'exo2',
      ennonce: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugit eum placeat consequatur doloremque officiis ducimus.',
      langage: 'php'}
  ];

  constructor(private router: Router) { }

  ngOnInit() {
  }

  addExo() {
    this.router.navigate(['admin/addexo']);
  }
}
