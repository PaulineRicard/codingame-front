import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExoAdminComponent } from './exo-admin.component';

describe('ExoAdminComponent', () => {
  let component: ExoAdminComponent;
  let fixture: ComponentFixture<ExoAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExoAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExoAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
