import {Component, Input, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UserService} from '../../service/user/user.service';
import {User} from '../../model/User';

@Component({
  selector: 'app-modal-update-user',
  templateUrl: './modal-update-user.component.html',
  styleUrls: ['./modal-update-user.component.css']
})
export class ModalUpdateUserComponent implements OnInit {
  @Input() user: object;
  closeResult: any;
  role: any;
  id: any;
  username: any;
  email: any;
  password: any;

  constructor(private modalService: NgbModal, private userService: UserService) {
  }

  ngOnInit() {
    // @ts-ignore
    console.log(this.user.username);
    // @ts-ignore
    this.username = this.user.username;
    // @ts-ignore
    this.email = this.user.email;
    // @ts-ignore
    this.password = this.user.password;
    // @ts-ignore
    this.role = this.user.role;
    // @ts-ignore
    this.id = this.user.id;
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  save() {
    // TODO envoyer les données vers le service pour mise a jour de la bdd!!
    const newUser = new User(this.id, this.username, this.email, this.password, this.role);
    console.log(newUser);
    this.userService.updateUser(newUser).then(result => {
      console.log(result);
    }).catch(reason => {
      console.log(reason);
    });
    this.modalService.dismissAll('click save');
  }
}
