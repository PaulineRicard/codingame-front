import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LangageAdminComponent } from './langage-admin.component';

describe('LangageAdminComponent', () => {
  let component: LangageAdminComponent;
  let fixture: ComponentFixture<LangageAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LangageAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LangageAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
