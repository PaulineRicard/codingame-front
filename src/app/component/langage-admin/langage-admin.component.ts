import { Component, OnInit } from '@angular/core';
import {LangageService} from '../../service/langage/langage.service';

@Component({
  selector: 'app-langage-admin',
  templateUrl: './langage-admin.component.html',
  styleUrls: ['./langage-admin.component.css']
})
export class LangageAdminComponent implements OnInit {
  langages;
  name: any;
  src: any;


  constructor(private langageService: LangageService) { }

  ngOnInit() {
    this.getlangages();
  }

  addLangage() {
    // Todo recuperer info input + appeler le service pour ajouter en bdd !!
    const body = {
      name: this.name,
      src: this.src
    };
    console.log(this.name, this.src + '######################')
    this.langageService.create(body)
      .then(result => {
      console.log(result);
    }).catch(reason => {
      console.log(reason);
    });
  }
  getlangages() {
    this.langageService.getAll().then(result => {
      console.log(result);
      this.langages = result;
      // todo recharger le component !!!
    }).catch(reason => {
      console.log(reason);
    });
  }
}
